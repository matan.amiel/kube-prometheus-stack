# kube-prometheus-stack

![alt text](https://global-uploads.webflow.com/5c9200c49b1194323aff7304/63d0c90032a91d9354139b02_Infra_Monitoring-570x330.png)

Installs the kube-prometheus stack, a collection of Kubernetes manifests, Grafana dashboards, and Prometheus rules combined with documentation and scripts to provide easy to operate end-to-end Kubernetes cluster monitoring with Prometheus using the Prometheus Operator.

## Tools and Data sources:
- Grafana
- Prometheus
- AlertManager
- Loki
- Ptomtail

**step 1:** Create k3d cluster
```
  k3d cluster create monitoring --api-port 6550 --agents 1 --k3s-arg "--disable=traefik@server:0" --k3s-arg "--disable=servicelb@server:0" --no-lb --wait
```

**step 2:** Add Helm Repository
```
  helm repo add argo https://argoproj.github.io/argo-helm
  helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
  helm repo add grafana https://grafana.github.io/helm-charts
  helm repo update 
```

**step 3:** Create namespaces
```
  kubectl create ns monitoring
```

**step 4:** Install kube-prometheus-stack
```
  helm upgrade --install -f values.yaml kube-prometheus-stack prometheus-community/kube-prometheus-stack -n monitoring
```
**step 4.1:** Install Service Sonitor as a test 
```
  kubectl apply -f servicemonitor.yaml -n monitoring
```

**step 5:** Install Loki
```
  helm upgrade --install -f loki-distributed.yaml loki grafana/loki-distributed -n monitoring
```

**step 6:** Install Ptomtail
```
  helm upgrade --install -f promtail.yaml promtail grafana/promtail -n monitoring
```